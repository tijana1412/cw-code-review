class Person():
    def __init__(self, unoqueId, firstName, lastName, birthday, accessLevel, inSchool):
        self.uniqueId = unoqueId
        self.firstName = firstName
        self.lastName = lastName
        self._birthday = birthday
        self._accessLevel = accessLevel
        self._inSchool = inSchool

    def isinSchool(self):
        return True

    def goHome(self):
        return

class Student(Person):
    def __init__(self,clas ,classTeacher, unoqueId, firstName, lastName, birthday, accessLevel, inSchool, difficulty, name):
        super().__init__( unoqueId, firstName, lastName, birthday, accessLevel, inSchool)
        super().__init__(difficulty, name)
        self.clas = clas
        self.classTeacher = classTeacher

    def study(self):
        return

class Subject():
    def __init__(self, difficulty, name):
        self.difficulty = difficulty
        self.name = name

class Teacher(Person):
    def __init__(self, unoqueId, firstName, lastName, birthday, accessLevel, inSchool, subject, cabinet):
        super().__init__(unoqueId, firstName, lastName, birthday, accessLevel, inSchool)
        self.subject = subject
        self.cabinet = cabinet

    def teach(self):
        return
    def getPromotion(self):
        return

