def_chars = "abcde"


def first_half(in_chars):
    out = ""
    levels = len(in_chars) * 2 + 1
    prev_char = ""
    for index, char in enumerate(in_chars):
        current_char = char
        temp = ""
        for i in range(index):
            temp += in_chars[i]
        out += temp + current_char * (levels - index * 2) + temp[::-1] + "\n"
        prev_char = current_char
    return out + in_chars


def top_view(characters):
    return first_half(characters[0:-1]) + characters[-1] + first_half(characters[0:-1])[::-1]


print(top_view(def_chars))